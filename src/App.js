import logo from "./logo.svg";
import "./App.css";
import useStateDemo from "./basic-hooks/useStateDemo";
import useEffectDemo from "./basic-hooks/useEffectDemo";
import useRefDemo from "./basic-hooks/useRefDemo";
import useMemoDemo from "./basic-hooks/useMemoDemo";
import useWindowSize from "./hooks/usewindowSize";
import useGeoLocation from "./hooks/useGeoLoaction";
import useForm from "./hooks/useForm";
import useArray from "./hooks/useArray";
import useFetch from "./hooks/useFetch";
import useCopyToClipboard from "./hooks/useCopyToClipboard";
import { CopyToClipboard } from "react-copy-to-clipboard";
import useLocalStorage from "./hooks/useLocalStorage";
import { useState ,useRef} from "react";
import useSessionStorage from "./hooks/useSessionStorage";
import useKeyPress from "./hooks/useKeyPress";
import useToggle from "./hooks/useToggle";
import useCallbackDemo from "./basic-hooks/useCallbackDemo";


function App() {

  //Use-State Hook Demo
  // const [text,setText]=useStateDemo()
  //   return(
  //   <>
  //   <h1><center>useState Hook Demo</center></h1>
  //   <h2>Name:{text.Name}</h2>
  //   <h2>Age:{text.age}</h2>
  //   <button onClick={()=>setText(previous=>{
  //     return{...previous,
  //       Name:'Keval-Mehta',
  //       age:21}
  //   })}>Change the Text and Age </button>
  //   </>
  // )


  //Use-Effect Hook Demo
//   const[value,result,setValue,setResult]=useEffectDemo()
//   return(
//     <>
//     <h1><center>useEffect Hook Demo</center></h1>
//     <h2>Value (Add 2)::{value}</h2>
//     <h2>Result((value*2)+(value*2))::{result}</h2>
//     <button onClick={()=>setValue(value+2)}>Click me for get Result!</button>
//     </>
// )

  //Use-Ref Hook Demo
  // const[changeValue,focusDemo]=useRefDemo();
  // return(
  //   <>
  //    <h1><center>useRefHook Demo</center></h1>
  //   <input type="text" ref={focusDemo}/>
  //   <br/>
  //   <br/>
  //   <button onClick={changeValue}>Click here For Ref Demo</button>
  //   </>
  // )

  //Use-Memo Hook Demo
//   const [value,setValue,count,cubeValueResult,setCount]=useMemoDemo();
//   return(
//     <>
//     <h1><center>useMemo Hook Demo</center></h1>
//     Enter Value:<input type="text" 
//     value={value} 
//     onChange={(e)=>setValue(e.target.value)}/>
//     <h2>Result of Value:{cubeValueResult}</h2>

//     <h2>Count:{count}</h2>
//     <button onClick={()=>{setCount(count+3);
//        console.log('Counter Updated..')}}>Click me of Update Count Value</button>
//     </>
// )

//Use-CallBack Demo
const [count,setCount,number,setNumber,incrementCounter,decrementCounter,incrementNumber,]=useCallbackDemo();
return (
  <div>
    <h1><center>use CallBack Hook Demo</center></h1>
    Count: {count}
    <br/>
    Number:{number}
    <br/>
    <button onClick={incrementCounter}>
      Increase counter
    </button>
    <br/>
    <button onClick={decrementCounter}>
       Decrease Counter
    </button>
    <br/>
    <button onClick={incrementNumber}>
      Increase Number
    </button>
  </div>
)




  // //Task:1 Fetch data from server
  // const { data, loading, error, refetchData } = useFetch(
  //    "https://v2.jokeapi.dev/joke/Any"
  // );

  // if (loading)
  // return <h1> LOADING Data...</h1>;

  // if (error)
  // console.log(error);

  // return (
  //   <div>
  // <h1>Custom Hook For Fetch Data From Server</h1>
  //     <h1>

  //       {data?.setup} ??{data?.delivery}
  //       {/* {data?data.setup:data.delivery} */}

  //     </h1>

  //     <button onClick={refetchData}> Get Another Data</button>
  //   </div>
  // );

  // //Task-2 Create hook for get window, height and screen resolution
  // const size=useWindowSize();
  // // console.log(size.width)
  // // console.log(size.height)
  // return (
  //   <div>
  //       <h1>Custom  Hook For Screen Width and Height</h1>
  //     Screen Width: {size.width}px
  //     <br/>
  //     Screen Height: {size.height}px
  //   </div>
  // );

  // //Task-3 Get user latitude and logitude
  // const location = useGeoLocation();
  // return (
  //   <div>
  //     <h1>Custom Hook For Geolocation</h1>
  //     {location.loaded
  //       ? JSON.stringify(location)
  //       : "Location data not available yet."}
  //   </div>
  // );

  //Task:4 State Validation
  // const formLogin = () => {

  //   console.log("Callback function when form is submitted!");
  //   console.log("Form Values ", values);
  // }

  // const {handleChange, values,errors,handleSubmit} = useForm(formLogin);
  // return (
  //   <div >
  //     <h1>Custom Hook for State Validation</h1>
  //     <form onSubmit={handleSubmit}>

  //     Username:<input type="text"  required name="username" placeholder="Enter Your Username"  onChange={handleChange}   />
  //     {
  //       <h3>{errors.username}</h3>
  //     }
  //     <input type="submit" value="Submit"/>
  //     </form>

  //   </div>
  // );

  // //Task:5-A LocalStorage Hook
  // const [name, setName] = useLocalStorage('username', 'John');
  //   return (
  //     <div>
  //  <h2>Enter Text For Store Locally:</h2>
  //  <br/><input
  //       value={name}
  //       onChange={(e) => {
  //         setName(e.target.value);
  //       }}
  //     />
  //     </div>

  //   );

  //Task:5-B sessionStorage Hook
  // const[email,setEmail]=useSessionStorage('email','abc@gmail.com')
  // return(
  //   <div>
  //     <h1>Custom Hook for Session Storage</h1>
  //     <h2>Enter the Email</h2>
  //     <br/>
  //     <input
  //     value={email}
  //     onChange={(e) => {
  //       setEmail(e.target.value);
  //     }}
  //     />
  //   </div>
  // )

  //Task:6 Perform Array Operations
  // const { array, setArray, push, remove, filter, update, clear } = useArray([
  //  11,22,45,78,4,25,10,6,41,498
  // ])
  // return (
  //   <div>
  //     <div>{array.join(", ")}</div>
  //     <button onClick={() => push(73)}>Add 73</button>
  //     <br/>
  //     <br/>
  //     <button onClick={() => update(1, 8)}>Change Second Element To 8</button>
  //     <br/>
  //     <br/>
  //     <button onClick={() => remove(1)}>Remove Second Element</button>
  //     <br/>
  //     <br/>
  //     <button onClick={() => filter(n => n < 45)}>
  //       Keep Numbers Less Than 45
  //     </button>
  //     <br/>
  //     <br/>
  //     <button onClick={() => setArray([48,101,5269])}>Set To New Array</button>
  //     <br/>
  //     <br/>
  //     <button onClick={clear}>Clear</button>
  //   </div>
  // )

  //Task:7 Copy to Clipboard
  // const{handleValue,value,copied,setCopied}=useCopyToClipboard()
  //   return (
  //     <div>
  //       <div>
  //         <h1>Custom Hook for Copy to ClipBoard</h1>
  //         <input
  //           type="text"
  //           value={value}
  //           onChange={handleValue}
  //           placeholder="Type Something to Copy"
  //           required
  //         />
  //         <br/>
  //         <br/>
  //         <CopyToClipboard text={value} onCopy={() => setCopied(true)}>
  //           <button>Click to Copy</button>
  //         </CopyToClipboard>
  //         {copied ? <p>Entered Text is Copied !</p> :<p>Not Copied Yet</p> }
  //       </div>
  //       <div>
  //         <input type="text" placeholder="Paste the Clipboard" />
  //       </div>
  //     </div>

  //   )

  //Extra Task: Custom KeyPress Hook
  // const happyPress = useKeyPress("h");
  // const sadPress = useKeyPress("s");
  // const robotPress = useKeyPress("r");
  // const foxPress = useKeyPress("f");
  // const batPress =useKeyPress('b')
  // const applePress=useKeyPress('a')
  // return (
  //   <div>
  //     <h1>Custom KeyPress Hook</h1>
  //     <div>h=happyh, s=sad, r=robot, f=fox ,a=apple ,b=bat</div>
  //     <div>
  //       {happyPress && "😊"}
  //       {sadPress && "😢"}
  //       {robotPress && "🤖"}
  //       {foxPress && "🦊"}
  //       {batPress && " 🏏 "}
  //       {applePress && '🍎'}
  //     </div>
  //   </div>
  // );

  //Extra Task:Custom Toggle Hook

  //   const [TextChange, setTextChange] = useToggle();

  //       return (
  //           <div>
  //             <h1>Custom Hook For Toggle Event</h1>
  //               <p>{TextChange ? 'Text is Toggled after the Button Click' : 'Your Text is not Toggle Yet...'}</p>
  //             <button onClick={setTextChange}>Click For Toggle</button>
  //           </div>

  //         );

  
}

export default App;
