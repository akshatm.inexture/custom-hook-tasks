
import React,{useState, useEffect } from 'react';

 const useLocalStorage = (key, defaultValue) => {
  const storeValue = localStorage.getItem(key);
  const initial = storeValue ? JSON.parse(storeValue) : defaultValue;
  console.log(initial)
  const [value, setValue] = useState(initial);

  useEffect(() => {
    localStorage.setItem('Company','Inexture Solutions ')
    localStorage.setItem('Name','Akshat')
    localStorage.setItem(key, JSON.stringify(value));
  }, [key, value]);

  return [value, setValue];
};

export default useLocalStorage