import { useState, useEffect } from "react";

function useWindowSize() {

    const [windowsize,setWindowSize]=useState({
        width:undefined,
        height:undefined,
    })

    useEffect(()=>{
        function windowResize(){

            //Get Window Height and Width(screen Resolution)
            setWindowSize({
                width:window.innerWidth,
                height:window.innerHeight,
            });
        }

        //resize is event and windowResize is function
         window.addEventListener("resize", windowResize);
             windowResize();
       
            return () => window.removeEventListener("resize", windowResize);
    },[])
    return windowsize;
}

export default useWindowSize