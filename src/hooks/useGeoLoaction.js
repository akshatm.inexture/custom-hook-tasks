import React, { useState, useEffect } from "react";

const useGeoLocation=()=>{

    const[location,setLocation]=useState({
        loaded:false,
        cordinates:{lat:" ",lng:''}
    });

const onSuccess=(location)=>{
    setLocation({
        loaded:true,
        cordinates:{
            lat:location.coords.latitude,
            lng:location.coords.longitude}
    });
}

const onError = (error) => {
    setLocation({
        loaded: true,
        error: {
            message: error.message,
        },
    });
};


    useEffect(() => {
        if (!("geolocation" in navigator)) {
            onError({
                message: "Geolocation not supported",
            });
        }
       else{
        navigator.geolocation.getCurrentPosition(onSuccess, onError);
       }
            
        
        
    }, []);

    return location;
}


export default useGeoLocation
