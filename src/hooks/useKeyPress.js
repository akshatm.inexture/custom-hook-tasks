import React, { useState,useEffect } from 'react'

function useKeyPress(targetKey) {
    const[keyPress,setKeyPress]=useState(false);

    function keyDownHandler({ key }) {
        if (key === targetKey) {
          setKeyPress(true);
        }
      }
      const keyUpHandler=({ key })=> {
        if (key === targetKey) {
          setKeyPress(false);
        }
      };

    useEffect(()=>{
    window.addEventListener('keyup',keyUpHandler)
      window.addEventListener('keydown',keyDownHandler)

      return()=>{
        window.removeEventListener('keyup',keyUpHandler)
        window.removeEventListener('keydown',keyDownHandler)
      };
    },[]);
    return keyPress;
 
}

export default useKeyPress