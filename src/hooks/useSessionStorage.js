import React,{useState,useEffect} from "react";

function useSessionStorage(key,initialValue){
    
    const stored=sessionStorage.getItem(key)
    const newValue = stored ? JSON.parse(stored) : initialValue;
    console.log(newValue)
    const[value,setValue]=useState(newValue)

    useEffect(()=>{
        sessionStorage.setItem('name','Akshat Mehta')
        sessionStorage.setItem(key, JSON.stringify(value))
    },[key,value])

    return [value, setValue];
}
export default  useSessionStorage