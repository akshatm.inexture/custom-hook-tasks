import { useState } from "react"

function useArray(defaultValue) {
  const [array, setArray] = useState(defaultValue)

  function push(element) {
    setArray(value => [...value, element])
  }

  function filter(valueCondition) {
    setArray(value => value.filter(valueCondition))
  }

  function update(index, newElement) {
    setArray(value => [
      ...value.slice(0, index),
      newElement,
      ...value.slice(index + 1, value.length),
    ])
  }

  function remove(index) {
    setArray(value => 
      [...value.slice(0, index),
      ...value.slice(index + 1, value.length)
    ])
  }

  function clear() {
    setArray([])
  }

  return { array, setArray, push, filter, update, remove, clear }
}

export default useArray