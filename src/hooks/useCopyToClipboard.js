import React,{useState} from 'react'

function useCopytoClipBoard() {
    const [copied, setCopied] = useState(false);
    const [value, setValue] = useState("");

  const handleValue = (event) => {
    setCopied(false)
    setValue(event.target.value);
  };
  return{handleValue,setCopied,value,copied}
}

export default useCopytoClipBoard