import React,{useState} from 'react'

function useStateDemo() {
const[text,setText]=useState({
   Name:'Akshat-Mehta',
   age:20,
   enrollment_no:180320107045,
});
 return[text,setText]
}

export default useStateDemo