import React,{useState,useCallback} from 'react'

function useCallbackDemo() {
    const [count, setCount] = useState(0)
    const funccount = new Set();
    const [number, setNumber] = useState(0)
   
    const incrementCounter = useCallback(() => {
        setCount(count + 1)
      
        console.log(count)
        console.log(number)

    }, [count]) 
    const decrementCounter = useCallback(() => {
        setCount(count - 1)
        console.log(count)
        console.log(number)
      
      }, [count]) 
     
     const incrementNumber =  useCallback(() => {
        setNumber(number + 1)
        console.log(count)
       console.log(number)

      }, [number])
     
  funccount.add(incrementCounter);
  console.log(`increment-counter:${funccount.size}`);
  funccount.add(decrementCounter);
  console.log(`decrement-counter:${funccount.size}`);
  funccount.add(incrementNumber);
  console.log(`increment-number:${funccount.size}`);

  return[count,setCount,number,setNumber,incrementCounter,decrementCounter,incrementNumber]
}

export default useCallbackDemo