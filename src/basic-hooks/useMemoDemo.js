import React,{useState,useMemo} from 'react'

function useMemoDemo() {
 const[value,setValue]=useState("");
 const[count,setCount]=useState(0);

//Without useMemo
//  const cubeValueResult=cubeValue(value)

//with useMemo
 const cubeValueResult =useMemo(()=>{
    return cubeValue(value);
 },[value])

 function cubeValue(value){
    if(value!=""){
        console.log("Cubing will be done!");
        return Math.pow(value, 3);
     }

  }
  return  [value,setValue,count,cubeValueResult,setCount]
}

export default useMemoDemo