import React,{useState,useEffect} from 'react'

function useEffectDemo() {
  const[value,setValue]=useState(0);
  const[result,setResult]=useState(0);

useEffect(()=>{
  setResult(()=>(value*2)+(value*2))
},[value]);

return [value,result,setValue,setResult]
}
export default useEffectDemo