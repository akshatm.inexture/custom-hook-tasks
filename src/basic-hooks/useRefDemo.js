import React,{useRef} from 'react'

function useRefDemo() {

     const focusDemo=useRef(null);
     const changeValue=()=>{
    focusDemo.current.value="Inexture Solutions";
    focusDemo.current.focus();
    focusDemo.current.style.fontSize='35px';
    focusDemo.current.style.color='red';
    focusDemo.current.style.fontFamily='cursive';
}
return [changeValue,focusDemo];
}

export default useRefDemo